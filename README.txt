Development is being done on the 6.x-1.x and 7.x-1.x branches.

Please try switching to one of those branches:

> git checkout 6.x-1.x

or

> git checkout 7.x-1.x